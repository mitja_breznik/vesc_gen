#include "ch.h" // ChibiOS
#include "hal.h" // ChibiOS HAL
#include "mc_interface.h" // Motor control functions
#include "hw.h" // Pin mapping on this hardware
#include "timeout.h" // To reset the timeout
 
//define application mode
#define INACTIVE 0
#define ACTIVE_DUMMY 1
#define WIND_TURBINE 2


//define dummy active values
#define ACTIVE_DUMMY_MAX_SPEED 5000.0
#define ACTIVE_DUMMY_MIN_SPEED 2000.0
#define ACTIVE_DUMMY_INC 2.0

//define wind turbine constants
#define DEFAULT_WIND_SPEED_IN_RPM 5000.0
#define INVERSE_WIND_COEFFICIENT (1.0/0.001)
#define CURR_RC_FILTER_COEF (0.01)

#define SM_WIND_TURBINE_INIT 0
#define SM_WIND_TURBINE_MODE_ACTIVE 1

// Example thread
static THD_FUNCTION(example_thread, arg);
static THD_WORKING_AREA(example_thread_wa, 2048); // 2kb stack for this thread
 
void app_example_init(void) {
	
	// Start the example thread
	chThdCreateStatic(example_thread_wa, sizeof(example_thread_wa),
		NORMALPRIO, example_thread, NULL);
}
 
static THD_FUNCTION(example_thread, arg) {
	(void)arg;
 
	chRegSetThreadName("APP_EXAMPLE");
 	
	static int mode_current = INACTIVE;
	static int mode_req = WIND_TURBINE;

	static float dummy_speed_req = ACTIVE_DUMMY_MIN_SPEED;
	static int accel_dir = 1;

	//define wind turbine variables
	static float wind_speed_in_rpm = DEFAULT_WIND_SPEED_IN_RPM;
	static float motor_current = 0;
	static float motor_current_old = 0;
	static float motor_set_speed = 0;

	static int wind_turbine_state = SM_WIND_TURBINE_INIT;
	static float init_rampup_speed = ACTIVE_DUMMY_MIN_SPEED;


	for(;;) {

	//set requested mode
	//mode_current = config.custom_app_mode;
	mode_current = WIND_TURBINE;

	//main modes SM
	switch (mode_current)
	{
/*************************************************************************************************************/
// INACTIVE STATE 
/*************************************************************************************************************/
		case INACTIVE:
		mc_interface_release_motor();
		break;
/*************************************************************************************************************/
// END OF -> INACTIVE STATE 
/*************************************************************************************************************/


/*************************************************************************************************************/
// DUMMY STATE
/*************************************************************************************************************/
		case ACTIVE_DUMMY:
		
		//incrementing
			if((dummy_speed_req < ACTIVE_DUMMY_MAX_SPEED) && (accel_dir == 1))
			{
				dummy_speed_req = dummy_speed_req + ACTIVE_DUMMY_INC; 
			}
			else if(accel_dir == 1)
			{
			accel_dir = 0;	
			}

		//decrementing
			if((dummy_speed_req > ACTIVE_DUMMY_MIN_SPEED) && (accel_dir == 0))		
			{
				dummy_speed_req = dummy_speed_req - ACTIVE_DUMMY_INC; 
			}
			else if(accel_dir == 0)
			{
			accel_dir = 1;	
			}

		mc_interface_set_pid_speed(dummy_speed_req);
		break;
/*************************************************************************************************************/
// END OF -> DUMMY STATE
/*************************************************************************************************************/


/*************************************************************************************************************/
// WIND TURBINE
/*************************************************************************************************************/
		case WIND_TURBINE:
		
		if(wind_turbine_state == SM_WIND_TURBINE_INIT)
		{
		//slowly ramp up to maximum speed
			if(init_rampup_speed < DEFAULT_WIND_SPEED_IN_RPM)
			{
				init_rampup_speed = init_rampup_speed + ACTIVE_DUMMY_INC;
			}
			else
			{
				wind_turbine_state = SM_WIND_TURBINE_MODE_ACTIVE;
			}

			mc_interface_set_pid_speed(init_rampup_speed);
			motor_current = mc_interface_get_tot_current_filtered();
		}
		else
		{
 
		//get current value
		motor_current = motor_current_old+CURR_RC_FILTER_COEF*(mc_interface_get_tot_current_filtered()-motor_current_old);
		motor_current_old = motor_current;

		//calculate wind turbine model
		motor_set_speed = wind_speed_in_rpm - motor_current*INVERSE_WIND_COEFFICIENT;

		//set motor speed
		mc_interface_set_pid_speed(motor_set_speed);
		}

		break;
/*************************************************************************************************************/
// END OF -> WIND TURBINE
/*************************************************************************************************************/


/*************************************************************************************************************/
		default:
		mc_interface_release_motor();

	} 
			
	
		// Run this loop at 500Hz
		chThdSleepMilliseconds(2);
 
		// Reset the timeout
		timeout_reset();
	}
}
